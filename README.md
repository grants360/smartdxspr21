# SmartDX

---

Download and copy SmartDx folder in your project files.

> [!IMPORTANT]Make sure to follow the below guide for successfully integrating **_Smartapps SmartDX_** into your project. Please note this guide is only suitable for projects running withing Smartapps ecosystem. We are working on guide to implement the same onto other projects, however for now, to implement SmartDX onto your client project, please contact below contacts.

## Admin Contacts

- [Sneh Malpani](mailto:sneh@smartapps.com.au "Sneh Malpani")
- [Nikhil Sharma](mailto:nikhil@smartapps.com.au "Nikhil Sharma")
- [Anurag Malpani](mailto:anurag@smartapps.com.au "Anurag Malpani")

# Self-Installation

---

## Project Files to Modify

### sfdx-proj.json

Modify your project file to include `{"path":"smartdx", "default":false}`.

```json
//Example:
{
  "packageDirectories": [
    {
      "path": "force-app",
      "default": true
    },
    {
      "path": "smartdx",
      "default": false
    }
  ],
  "namespace": "",
  "sfdcLoginUrl": "https://login.salesforce.com",
  "sourceApiVersion": "48.0"
}
```

### .gitignore

> Add `server.key` to avoid uploading decrypted key to online repository.

### .forceignore

> Add `smartdx/certificates` to ignore it while deployment.

## Add Repository Variables

| Variable Name               | Variable Value                         |
| --------------------------- | -------------------------------------- |
| SMARTDX_PROD_CONSUMER_KEY   | Refer LastPass or ask Admin Contacts   |
| SMARTDX_PROD_USER           | smartdx@smartapps.com.au               |
| SMARTDX_KEY                 | Refer LastPass or ask Admin Contacts   |
| SMARTDX_ENCRYPTED_KEY       | smartdx/certificates/secure-server.key |
| SMARTDX_PROD_INSTANCE_URL   | https://login.salesforce.com           |
| \$SMARTDX_TEST_INSTANCE_URL | https://test.salesforce.com            |

> [!NOTE] You might have to add similar variables for different environments where you want to deploy automatically. For example: `$SMARTDX_TEST_CONSUMER_KEY` will be Connected App Consumer installed in your Test Environment and `$SMARTDX_TEST_USER` will be the username of deployment user in Test Environment.

### Sample YAML File

```yml
image: smartappsaustralia/sfdx:latest

pipelines:
  default:
    - step:
        name: Scratch Org Validation
        script:
          # Test Docker Image
          - sfdx force --help
          # Checking OpenSSL Installation
          - openssl version
          # Decrypt Key
          - openssl aes-256-cbc -md sha512 -pbkdf2 -iter 100000 -salt -k "$SMARTDX_KEY" -in "$SMARTDX_ENCRYPTED_KEY" -out server.key -d
          # Authenticate Dev Hub with JWT
          - sfdx force:auth:jwt:grant -u "$SMARTDX_PROD_USER" -f server.key -i "$SMARTDX_PROD_CONSUMER_KEY" -r "$SMARTDX_PROD_INSTANCE_URL" -a prod
          # Create Scratch Org and push Source
          - sfdx force:org:create -f config/project-scratch-def.json -a scratchTest -d 1 -v prod --json --loglevel fatal
          # Push the Source files mentioned in Package Directories at sfdx-project.json
          - sfdx force:source:push -u scratchTest --loglevel fatal
          # Delete Scratch Org
          - sfdx force:org:delete -u scratchTest --noprompt
  pull-requests: #These commands run for all branches unless specified otherwise
    "**/**":
      - step:
          name: PR Validation
          script:
            # Test Docker Image
            - sfdx force --help
            # Checking OpenSSL Installation
            - openssl version
            # Decrypt Key
            - openssl aes-256-cbc -md sha512 -pbkdf2 -iter 100000 -salt -k "$SMARTDX_KEY" -in "$SMARTDX_ENCRYPTED_KEY" -out server.key -d
            # Authenticate Dev Hub with JWT
            - sfdx force:auth:jwt:grant -u "$SMARTDX_PROD_USER" -f server.key -i "$SMARTDX_PROD_CONSUMER_KEY" -r "$SMARTDX_PROD_INSTANCE_URL" -a prod
            # Create Scratch Org and push Source
            - sfdx force:org:create -f config/project-scratch-def.json -a scratchTest -d 1 -v prod --json --loglevel fatal
            # Push the Source files mentioned in Package Directories at sfdx-project.json
            - sfdx force:source:push -u scratchTest --loglevel fatal
            # Delete Scratch Org
            - sfdx force:org:delete -u scratchTest --noprompt
  branches:
    develop:
      - step:
          name: Deploy into the sandbox with Push on specified Branch.
          script:
            # Decrypt Key
            - openssl aes-256-cbc -md sha512 -pbkdf2 -iter 100000 -salt -k "$SMARTDX_KEY" -in "$SMARTDX_ENCRYPTED_KEY" -out server.key -d
            # Authenticate Test Environment Sandbox
            # - sfdx force:auth:jwt:grant -u "$SMARTDX_TEST_USER" -f server.key -i "$SMARTDX_TEST_CONSUMER_KEY" -r "$SMARTDX_TEST_INSTANCE_URL" -a test
            # Deploy and Run Tests on Test Environment Sandbox
            # - sfdx force:source:deploy -p force-app -u $SMARTDX_TEST_USER -l RunLocalTests -u test -w 60 --verbose --json
```

# Smart Apps Code Review

code-review folder defines ruleset for {PMD}, {CodeScan}, {SonarQube}

## Configure VS Code to run the rule sets.

Strict Rulesets:
More Checks around Granular Coding

### Standard Rulesets:

Defined minimum rules which shouldn't be violated. Good For POCs and non-production packages.

### Test Class Coverage

### Javascript Testing Framework like JEST

Not All Tools are perfect and hence on top of it make sure to havoid some very granular warnings and errors but make sure to confirm.
