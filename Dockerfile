# use small node image
FROM node:alpine

# install git ca-certificates openssl openssh for CircleCI
# install jq for JSON parsing
RUN apk add --update --no-cache git openssh ca-certificates openssl jq gettext xmlstarlet curl

# install latest sfdx from npm
RUN node --version
RUN npm install sfdx-cli --global
RUN npm update sfdx-cli --global
# RUN sfdx plugins:install salesforcedx@latest-rc
# RUN set SFDX_REST_DEPLOY=false
RUN sfdx --version
RUN sfdx plugins --core

# revert to low privilege user
USER node